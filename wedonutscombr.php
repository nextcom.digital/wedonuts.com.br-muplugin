<?php
/**
 * We Donuts - mu-plugin
 *
 * PHP version 7
 *
 * @category  Wordpress_Mu-plugin
 * @package   WeDonuts
 * @author    Cimbre <contato@cimbre.com.br>
 * @copyright 2019 Cimbre
 * @license   Proprietary https://cimbre.com.br
 * @link      https://wedonuts.com.br
 *
 * @wordpress-plugin
 * Plugin Name: We Donuts - mu-plugin
 * Plugin URI:  https://wedonuts.com.br
 * Description: Customizations for wedonuts.com.br site
 * Version:     1.0.1
 * Author:      Cimbre
 * Author URI:  https://cimbre.com.br/
 * Text Domain: wedonuts
 * License:     Proprietary
 * License URI: https://cimbre.com.br
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Load Translation
 *
 * @return void
 */
add_action(
    'plugins_loaded',
    function () {
        load_muplugin_textdomain('wedonutscombr', basename(dirname(__FILE__)).'/languages');
    }
);

/**
 * Hide editor from all pages
 */
add_action(
    'admin_init',
    function () {
        remove_post_type_support('page', 'editor');
    }
);

/***********************************************************************************
 * Callback Functions
 **********************************************************************************/

 /**
  * Metabox for Page Slug
  *
  * @param bool  $display  Display
  * @param array $meta_box Metabox 
  *
  * @return bool $display
  *
  * @author Tom Morton
  * @link   https://github.com/CMB2/CMB2/wiki/Adding-your-own-show_on-filters
  */
function Cmb2_Metabox_Show_On_slug($display, $meta_box)
{
    if (!isset($meta_box['show_on']['key'], $meta_box['show_on']['value'])) {
        return $display;
    }

    if ('slug' !== $meta_box['show_on']['key']) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (!$post_id) {
        return $display;
    }

    $slug = get_post($post_id)->post_name;

    // See if there's a match
    return in_array($slug, (array) $meta_box['show_on']['value']);
}
add_filter('cmb2_show_on', 'Cmb2_Metabox_Show_On_slug', 10, 2);

/**
 * Gets a number of terms and displays them as options
 *
 * @param CMB2_Field $field CMB2 Field
 *
 * @return array An array of options that matches the CMB2 options array
 */
function Cmb2_getTermOptions($field)
{
    $args = $field->args('get_terms_args');
    $args = is_array($args) ? $args : array();

    $args = wp_parse_args($args, array('taxonomy' => 'category'));

    $taxonomy = $args['taxonomy'];

    $terms = (array) cmb2_utils()->wp_at_least('4.5.0')
        ? get_terms($args)
        : get_terms($taxonomy, $args);

    // Initate an empty array
    $term_options = array();
    if (!empty($terms)) {
        foreach ($terms as $term) {
            $term_options[ $term->term_id ] = $term->name;
        }
    }
    return $term_options;
}

/***********************************************************************************
 * Pages Custom Fields
 * ********************************************************************************/

/**
 * Front-page
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_wedonutscombr_frontpage_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'wedonutscombr_frontpage_hero_id',
                'title'         => __('Hero', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'wedonutscombr'),
                    'add_button'   =>__('Add Another Slide', 'wedonutscombr'),
                    'remove_button' =>__('Remove Slide', 'wedonutscombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#ffcc00', '#f0ac00', '#ff8c00', '#e80292', '#c7088e', '#e02a6c', '#c12e6a', '#9a5620', '#682f29', '#44204a', '#00da00', '#00cfb8', '#00d3eb'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Landscape)', 'wedonutscombr'),
                'description' => '',
                'id'          => 'bkg_image_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Portrait)', 'wedonutscombr'),
                'description' => '',
                'id'          => 'bkg_image_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        /******
         * Stores
         ******/
        $cmb_stores = new_cmb2_box(
            array(
                'id'            => 'wedonutscombr_frontpage_stores_id',
                'title'         => __('Stores', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Stores Background Color
        $cmb_stores->add_field(
            array(
                'name'       => __('Background Color', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'stores_bkg_color',
                'type'       => 'colorpicker',
                'default'    => 'transparent',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'alpha' => true, 
                            'palettes' => array('#ffffff', '#000000', '#ffcc00', '#f0ac00', '#ff8c00', '#e80292', '#c7088e', '#e02a6c', '#c12e6a', '#9a5620', '#682f29', '#44204a', '#00da00', '#00cfb8', '#00d3eb'),
                        )
                    ),
                ),
            )
        );
        
        //Stores Background Image
        $cmb_stores->add_field(
            array(
                'name'        => __('Background Image', 'wedonutscombr'),
                'description' => '',
                'id'          => $prefix . 'stores_bkg_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Stores Title
        $cmb_stores->add_field(
            array(
                'name'       => __('Title', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'stores_title',
                'type'       => 'textarea_code',
            )
        );

        //Stores Text
        $cmb_stores->add_field(
            array(
                'name'       => __('Text', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'stores_text',
                'type'       => 'textarea_code',
            )
        );

        //Stores Group
        $stores_id = $cmb_stores->add_field(
            array(
                'id'          => $prefix.'stores_items',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'  =>__('Store {#}', 'wedonutscombr'),
                    'add_button' =>__('Add Another Store', 'wedonutscombr'),
                    'remove_button'=>__('Remove Store', 'wedonutscombr'),
                    'sortable'     => true, // beta
                ),
            )
        );

        //Store Item Title
        $cmb_stores->add_group_field(
            $stores_id,
            array(
                'name' => __('Title', 'wedonutscombr'),
                'id'   => 'title',
                'type' => 'textarea_code',
            )
        );

        //Store Item Description
        $cmb_stores->add_group_field(
            $stores_id,
            array(
            'name' => __('Description', 'wedonutscombr'),
            'id'   => 'desc',
            'description' => '',
            'type' => 'textarea_code',
            )
        );

        //Store Map URL
        $cmb_stores->add_group_field(
            $stores_id,
            array(
                'name'       => __('Google Maps URL', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'map_url',
                'type'       => 'textarea_code',
            )
        );

        /**
         * Form
         */
        $cmb_form = new_cmb2_box(
            array(
                'id'            => 'wedonutscombr_frontpage_form_id',
                'title'         => __('Form', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Form Background Color
        $cmb_form->add_field(
            array(
                'name'       => __('Background Color', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'form_bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#ffcc00', '#f0ac00', '#ff8c00', '#e80292', '#c7088e', '#e02a6c', '#c12e6a', '#9a5620', '#682f29', '#44204a', '#00da00', '#00cfb8', '#00d3eb'),
                        )
                    ),
                ),
            )
        );
        
        //Form Background Image
        $cmb_form->add_field(
            array(
                'name'        => __('Background Image', 'wedonutscombr'),
                'description' => '',
                'id'          => $prefix . 'form_bkg_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Form Title
        $cmb_form->add_field(
            array(
                'name'       => __('Title', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'form_title',
                'type'       => 'textarea_code',
            )
        );

        //Form Text
        $cmb_form->add_field(
            array(
                'name'       => __('Text', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'form_text',
                'type'       => 'text',
            )
        );

        //Form Form Shortcode
        $cmb_form->add_field(
            array(
                'name'       => __('Form Shortcode', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'form_shortcode',
                'type'       => 'text',
            )
        );

        //Form Confirmation Title
        $cmb_form->add_field(
            array(
                'name'       => __('Form Confirmation Title', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'form_confirmation_title',
                'type'       => 'text',
            )
        );

        //Form Confirmation Text
        $cmb_form->add_field(
            array(
                'name'       => __('Form Confirmation Text', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'form_confirmation_text',
                'type'       => 'textarea_code',
            )
        );

        /**
         * Social
         */
        $cmb_social = new_cmb2_box(
            array(
                'id'            => 'wedonutscombr_frontpage_social_id',
                'title'         => __('Social', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Social Background Color
        $cmb_social->add_field(
            array(
                'name'       => __('Background Color', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'social_bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#ffcc00', '#f0ac00', '#ff8c00', '#e80292', '#c7088e', '#e02a6c', '#c12e6a', '#9a5620', '#682f29', '#44204a', '#00da00', '#00cfb8', '#00d3eb'),
                        )
                    ),
                ),
            )
        );
        
        //Social Background Image
        $cmb_social->add_field(
            array(
                'name'        => __('Background Image', 'wedonutscombr'),
                'description' => '',
                'id'          => $prefix . 'social_bkg_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Social Title
        $cmb_social->add_field(
            array(
                'name'       => __('Title', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'social_title',
                'type'       => 'textarea_code',
            )
        );

        //Social Title
        $cmb_social->add_field(
            array(
                'name'       => __('Text', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'social_text',
                'type'       => 'textarea_code',
            )
        );

        /**
         * Footer
         */
        $cmb_footer = new_cmb2_box(
            array(
                'id'            => 'wedonuts_frontpage_footer_id',
                'title'         => __('Footer', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Footer Background Color
        $cmb_footer->add_field(
            array(
                'name'       => __('Background Color', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'footer_bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#ffcc00', '#f0ac00', '#ff8c00', '#e80292', '#c7088e', '#e02a6c', '#c12e6a', '#9a5620', '#682f29', '#44204a', '#00da00', '#00cfb8', '#00d3eb'),
                        )
                    ),
                ),
            )
        );
        
        //Footer Background Image
        $cmb_footer->add_field(
            array(
                'name'        => __('Background Image', 'wedonutscombr'),
                'description' => '',
                'id'          => $prefix . 'footer_bkg_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
       
        //Footer Copyright
        $cmb_footer->add_field(
            array(
                'name'       => __('Text', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'footer_text',
                'type'       => 'textarea_code',
            )
        );
    }
);
